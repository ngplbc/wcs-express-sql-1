const express = require("express");
const morgan = require("morgan");
const multer = require("multer");
const fs = require("fs");
const path = require("path");

const app = express();
const router = express.Router();

app.use(morgan("dev"));

// load view engine
app.set("views", path.join(__dirname, "../views"));
app.set("view engine", "pug");

app.use("/", router);

// before being processed, the file will be placed in thetmp/' folder that will be automatically created by Multer.
const upload = multer({ dest:"tmp/" });

const checkSizeAndType = (req, res, next) => {
  const imgArr = req.files;
  const filteredArr = imgArr.filter(img => img.mimetype === "image/png" && img.size <= 3000000);
  imgArr.length !== filteredArr.length ?
    res.render("index", { error: "Please upload png files smaller than 3MB" })
    :
    next();
};

router.post("/monupload", upload.array("uploadedFiles"), checkSizeAndType, (req, res, next) => {
  req.files.forEach(file => {
    fs.rename(file.path, `./public/images/${file.originalname}`, (err) => {
      if (err) return res.sendStatus(500);
    })
  })
  res.render("index", { message: "File/s uploaded successfully" });
});

router.get("/", (req, res, next) => {
  res.render("index");
})

const PORT = 4000;

app.listen(PORT, () => console.log(`Server listening on port ${PORT}`));

